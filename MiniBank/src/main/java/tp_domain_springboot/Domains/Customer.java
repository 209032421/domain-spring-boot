package tp_domain_springboot.Domains;


import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int customerNumber;
    private String name;
    private String surname;
    private String contactNumber;
    private int accountNumber;

    public int getCustomerNumber() {
        return customerNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    private Customer() {
    }

    private Customer(Builder builder){
        this.customerNumber=builder.customerNumber;
        this.name=builder.name;
        this.surname=builder.surname;
        this.contactNumber=builder.contactNumber;
        this.accountNumber=builder.accountNumber;
    }

    public static class Builder{
        private int customerNumber;
        private String name;
        private String surname;
        private String contactNumber;
        private int accountNumber;

        public Builder customerNumber(int value){
            this.customerNumber = value;
            return this;
        }

        public Builder name(String value){
            this.name = value;
            return this;
        }

        public Builder surname(String value){
            this.surname = value;
            return this;
        }

        public Builder contactNumber(String value){
            this.contactNumber = value;
            return this;
        }

        public  Builder accountNumber(int value){
            this.accountNumber=value;
            return this;
        }

        public Customer build(){
            return new Customer(this);
        }

    }


}
