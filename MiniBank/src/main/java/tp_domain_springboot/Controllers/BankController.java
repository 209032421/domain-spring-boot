package tp_domain_springboot.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import tp_domain_springboot.Domains.Bank;
import tp_domain_springboot.Services.Impl.BankServiceImpl;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path="/bank")
public class BankController {

    @Autowired
    private BankServiceImpl bankService;

    @GetMapping(path="/all")
    public @ResponseBody
    Set<Bank> getAllBank()
    {
        return bankService.readAll();
    }


    @RequestMapping(value="/add", method = RequestMethod.POST)
    public ResponseEntity addBank(@RequestBody Bank bank){

        if(StringUtils.isEmpty(bank.getBranchCode()) || StringUtils.isEmpty(bank.getAddress()) || StringUtils.isEmpty(bank.getTelephone()))
        {
            return new ResponseEntity("Need extra information", HttpStatus.NO_CONTENT);
        }
        bankService.create(bank);
        return new ResponseEntity(bank, HttpStatus.OK);
    }

    @RequestMapping(value="/find/{branchCode}")
    public @ResponseBody ResponseEntity
    findBank(@PathVariable("branchCode") int branchCode)
    {
        Optional<Bank> bank = bankService.readByID(branchCode);

        if(!bank.isPresent())
        {
            return new ResponseEntity("No bank found for branch code:" + branchCode, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(bank, HttpStatus.OK);
    }

    @RequestMapping(value="/delete/{branchCode}", method = RequestMethod.DELETE)
    public void deleteBank(@PathVariable Bank branchCode)
    {
        bankService.delete(branchCode);
    }

    @RequestMapping(value="/deleteAll", method = RequestMethod.DELETE)

    public void deleteAllBank()
    {
        bankService.deleteAll();
    }


}
