package tp_domain_springboot.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import tp_domain_springboot.Domains.Account;
import tp_domain_springboot.Services.Impl.AccountServiceImpl;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path="/account")
public class AccountController {

    @Autowired
    private AccountServiceImpl accountService;

    @GetMapping(path="/all")
    public @ResponseBody
    Set<Account> getAllBatsman()
    {
        return accountService.readAll();
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public ResponseEntity addAccount(@RequestBody Account account){
        if(StringUtils.isEmpty(account.getAccountNumber()) || StringUtils.isEmpty(account.getBalance()) || StringUtils.isEmpty(account.getAccountType()))
        {
            return new ResponseEntity("No Empty Fields Allowed", HttpStatus.NO_CONTENT);

        }
        accountService.create(account);
        return new ResponseEntity(account, HttpStatus.OK);
    }

    @RequestMapping(value="/find/{accountNumber}")
    public @ResponseBody ResponseEntity
    /*Optional<Batsman>*/ findAccount(@PathVariable("accountNumber") int accountNumber)
    {
        Optional<Account> account = accountService.readByID(accountNumber);

        if(!account.isPresent())
        {
            return new ResponseEntity("No account found with account number:" + accountNumber, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(account, HttpStatus.OK);
    }


    @RequestMapping(value="/update", method = RequestMethod.PUT)
    public ResponseEntity updateAccount(@RequestBody Account account)
    {
        if(StringUtils.isEmpty(account.getAccountNumber()) || StringUtils.isEmpty(account.getBalance()) || StringUtils.isEmpty(account.getAccountType()))
        {
            return new ResponseEntity("No Empty Fields Allowed", HttpStatus.NO_CONTENT);
        }
        accountService.update(account);
        return new ResponseEntity(account, HttpStatus.OK);
    }

    @RequestMapping(value="/delete/{accountNumber}", method = RequestMethod.DELETE)
    public void deleteAccount(@PathVariable Account accountNumber)
    {
        accountService.delete(accountNumber);
    }


    @RequestMapping(value="/deleteAll", method = RequestMethod.DELETE)
    public void deleteAllAccount()
    {
        accountService.deleteAll();
    }



}
