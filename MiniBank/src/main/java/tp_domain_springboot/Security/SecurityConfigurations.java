package tp_domain_springboot.Security;


import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfigurations extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("client").password("{noop}client").roles("USER").and()
                .withUser("root").password("{noop}root").roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests()
                .antMatchers("**/bank/add").hasRole("ADMIN")
                .mvcMatchers("**/bank/update").hasRole("ADMIN")
                .antMatchers("**/customer/add").hasRole("ADMIN")
                .mvcMatchers("**/customer/update").hasRole("ADMIN")
                .antMatchers("**/account/add").hasRole("ADMIN")
                .mvcMatchers("**/account/update").hasRole("ADMIN")
                .anyRequest()
                .fullyAuthenticated()
                .and().httpBasic();
        httpSecurity.csrf().disable();
    }
}
