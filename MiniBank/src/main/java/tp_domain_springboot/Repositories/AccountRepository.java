package tp_domain_springboot.Repositories;

import org.springframework.data.repository.CrudRepository;
import tp_domain_springboot.Domains.Account;

public interface AccountRepository extends CrudRepository<Account, Integer> {
}
