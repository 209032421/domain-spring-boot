package tp_domain_springboot.Repositories;

import org.springframework.data.repository.CrudRepository;
import tp_domain_springboot.Domains.Bank;

public interface BankRepository extends CrudRepository<Bank, Integer> {
}
