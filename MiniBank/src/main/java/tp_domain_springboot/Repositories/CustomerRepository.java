package tp_domain_springboot.Repositories;

import org.springframework.data.repository.CrudRepository;
import tp_domain_springboot.Domains.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
}
