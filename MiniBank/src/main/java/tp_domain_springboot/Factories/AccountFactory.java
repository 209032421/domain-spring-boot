package tp_domain_springboot.Factories;

import tp_domain_springboot.Domains.Account;

public class AccountFactory {
    public static Account getAccount(int accountNumber, double balance, String accountType)
    {
        Account factoryAccount = new Account.Builder()
                .accountNumber(accountNumber)
                .balance(balance)
                .accountType(accountType)
                .build();
        return factoryAccount;
    }
}
