package tp_domain_springboot.Factories;

import tp_domain_springboot.Domains.Bank;

public class BankFactory {
    public static Bank getBank(int branchCode, String address, String telephone)
    {
        Bank factoryBank = new Bank.Builder()
                .branchCode(branchCode)
                .address(address)
                .telephone(telephone)
                .build();
        return factoryBank;
    }
}
