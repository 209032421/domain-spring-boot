package tp_domain_springboot.Factories;

import tp_domain_springboot.Domains.Customer;

public class CustomerFactory {
    public static Customer getCustomer(int customerNumber, String name, String surname, String contactNumber, int accountNumber)
    {
        Customer factoryCustomer = new Customer.Builder()
                .customerNumber(customerNumber)
                .name(name)
                .surname(surname)
                .contactNumber(contactNumber)
                .accountNumber(accountNumber)
                .build();
        return factoryCustomer;
    }
}
