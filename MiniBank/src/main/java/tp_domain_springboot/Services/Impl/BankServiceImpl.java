package tp_domain_springboot.Services.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tp_domain_springboot.Domains.Bank;
import tp_domain_springboot.Repositories.BankRepository;
import tp_domain_springboot.Services.BankService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class BankServiceImpl implements BankService{
    @Autowired
    private BankRepository bankRepository;

    @Override
    public Bank create(Bank bank)
    {
        return bankRepository.save(bank);
    }

    @Override
    public Optional<Bank> readByID(int branchCode)
    {
        return bankRepository.findById(branchCode);
    }

    @Override
    public Set<Bank> readAll()
    {
        Iterable<Bank> banks = bankRepository.findAll();
        Set bankSet = new HashSet();
        for(Bank bank:banks)
        {
            bankSet.add(bank);
        }
        return bankSet;
    }

    @Override
    public Bank update(Bank bank)
    {
        return bankRepository.save(bank);
    }

    @Override
    public void delete(Bank branchCode)
    {
        bankRepository.delete(branchCode);
    }

    @Override
    public void deleteAll()
    {
        bankRepository.deleteAll();
    }
}
