package tp_domain_springboot.Services.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tp_domain_springboot.Domains.Account;
import tp_domain_springboot.Repositories.AccountRepository;
import tp_domain_springboot.Services.AccountService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account create(Account account)
    {
        return accountRepository.save(account);
    }

    @Override
    public Optional<Account> readByID(int accountNumber)
    {
        return accountRepository.findById(accountNumber);
    }

    @Override
    public Set<Account> readAll()
    {
        Iterable<Account> accounts = accountRepository.findAll();
        Set accountSet = new HashSet();
        for(Account account:accounts)
        {
            accountSet.add(account);
        }
        return accountSet;
    }

    @Override
    public Account update(Account account)
    {
        return accountRepository.save(account);
    }

    @Override
    public void delete(Account accountNumber)
    {
        accountRepository.delete(accountNumber);
    }

    @Override
    public void deleteAll()
    {
        accountRepository.deleteAll();
    }
}
