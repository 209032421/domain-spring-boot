package tp_domain_springboot.Services.Impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tp_domain_springboot.Domains.Customer;
import tp_domain_springboot.Repositories.CustomerRepository;
import tp_domain_springboot.Services.CustomerService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer create(Customer customer)
    {
        return customerRepository.save(customer);
    }

    @Override
    public Optional<Customer> readByID(int customerNumber)
    {
        return customerRepository.findById(customerNumber);
    }

    @Override
    public Set<Customer> readAll()
    {
        Iterable<Customer> customers = customerRepository.findAll();
        Set customerSet = new HashSet();
        for(Customer customer:customers)
        {
            customerSet.add(customer);
        }
        return customerSet;
    }

    @Override
    public Customer update(Customer customer)
    {
        return customerRepository.save(customer);
    }

    @Override
    public void delete(Customer customerNumber)
    {
        customerRepository.delete(customerNumber);
    }

    @Override
    public void deleteAll()
    {
        customerRepository.deleteAll();
    }
}
