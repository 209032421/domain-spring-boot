package tp_domain_springboot.Services;

import tp_domain_springboot.Domains.Account;

import java.util.Optional;
import java.util.Set;

public interface AccountService {

    public Account create(Account account);

    public Optional<Account> readByID(int accountNumber);

    public Set<Account> readAll();

    public Account update(Account account);

    public void delete(Account accountNumber);

    public void deleteAll();
}
