# Domain + Spring Boot

You are required to implement the Services and Repository of your Domain using Spring Boot.

Deliverables:

Webservices that exposes your functionalities using Spring Boot.
MySQL Database to persist data  using Spring Boot.

You will be required to present your attempt.


To submit, commit your work on GITHUB and submit the link on Blackboard. NO FILES SHOULD BE UPLOADED.

You will be scored ZERO should you upload/submit any other thing apart from your GITHUB link.

